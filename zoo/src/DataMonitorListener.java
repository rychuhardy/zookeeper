/**
 * Created by ry on 30.05.2017.
 */

import java.util.List;

/**
 * Other classes use the DataMonitor by implementing this method
 */
public interface DataMonitorListener {
    /**
     * The existence status of the node has changed.
     */
    void setRunningState(boolean shouldBeRunning);

    /**
     * The ZooKeeper session is no longer valid.
     *
     * @param rc
     *                the ZooKeeper reason code
     */
    void closing(int rc);

    void childNumberUpdate(int count, List<String> results);
}