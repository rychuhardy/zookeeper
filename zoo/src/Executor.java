import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.IOException;
import java.util.List;

public class Executor
        implements Watcher, Runnable, DataMonitorListener
{
    private DataMonitor dm;
    private String exec[];
    private Process child;

    public Executor(String hostPort, String znode,
                    String exec[]) throws KeeperException, IOException {
        this.exec = exec;
        ZooKeeper zk = new ZooKeeper(hostPort, 3000, this);
        dm = new DataMonitor(zk, znode, this);
    }

    public void process(WatchedEvent event) {
        dm.process(event);
    }

    public void run() {
        try {
            synchronized (this) {
                while (!dm.dead) {
                    wait();
                }
            }
        } catch (InterruptedException e) {
        }
    }

    public void closing(int rc) {
        synchronized (this) {
            notifyAll();
        }
    }

    public void setRunningState(boolean shouldBeRunning) {
        killChild();
        if (shouldBeRunning) {
            try {
                System.out.println("Starting child");
                child = Runtime.getRuntime().exec(exec);
                new StreamWriter(child.getInputStream(), System.out);
                new StreamWriter(child.getErrorStream(), System.err);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void childNumberUpdate(int count, List<String> results) {
        if (count == -1) {
            killChild();
        }
        else {
            System.out.println("Children count: " + Integer.toString(count));
            if(results.isEmpty()) {
                System.out.println("Empty results");
            }
            results.forEach(System.out::println);
        }
    }

    private void killChild() {
        if (child != null) {
            System.out.println("Killing process");
            child.destroy();
            try {
                child.waitFor();
            } catch (InterruptedException e) {
            }
        }
        child = null;
    }
}