/**
 * A simple class that monitors the data and existence of a ZooKeeper
 * node. It uses asynchronous ZooKeeper APIs.
 */
import java.util.*;

import org.apache.zookeeper.*;
import org.apache.zookeeper.AsyncCallback.StatCallback;
import org.apache.zookeeper.KeeperException.Code;
import org.apache.zookeeper.data.Stat;

public class DataMonitor implements Watcher, AsyncCallback.ChildrenCallback, StatCallback {

    private ZooKeeper zk;
    private String znode;
    private DataMonitorListener listener;
    private int previousCount = -1;
    private boolean previousState = false;
    boolean dead;


    public DataMonitor(ZooKeeper zk, String znode, DataMonitorListener listener) {
        this.zk = zk;
        this.znode = znode;
        this.listener = listener;

        // Watch deleting the node or creating/deleting children nodes
        zk.getChildren(znode, true, this, null);
        zk.exists(znode, true, this, null);
    }

    public void process(WatchedEvent event) {
        String path = event.getPath();
        if (event.getType() == Event.EventType.None) {
            // We are are being told that the state of the
            // connection has changed
            switch (event.getState()) {
                case SyncConnected:
                    // In this particular example we don't need to do anything
                    // here - watches are automatically re-registered with
                    // server and any watches triggered while the client was
                    // disconnected will be delivered (in order of course)
                    break;
                case Expired:
                    // It's all over
                    dead = true;
                    listener.closing(KeeperException.Code.SessionExpired);
                    break;
            }
        }
        else {
            if (path != null) {
                if (path.equals(znode)) {
                    // Something has changed on the node
                    zk.exists(znode, true, this, null);
                }
                if (path.startsWith((znode))) { // intentional fall through
                    // Something has changed on  child nodes
                    zk.getChildren(znode, true, this, null);
                }
            }
        }
    }

    @Override
    public void processResult(int rc, String path, Object ctx, List<String> children) {

        boolean exists;
        switch (rc) {
            case Code.Ok:
                exists = true;
                break;
            case Code.NoNode:
                exists = false;
                break;
            case Code.SessionExpired:
            case Code.NoAuth:
                dead = true;
                listener.closing(rc);
                return;
            default:
                // Retry errors
                zk.getChildren(znode, true, this, null);
                return;
        }
        if(exists) {
            Stack<String> toVisit = new Stack<>();
            List<String> results = new ArrayList<>();
            children.forEach(ch -> toVisit.add(znode + "/" + ch));

            while(!toVisit.isEmpty()) {
                String current = toVisit.pop();
                try {
                    results.add(current);
                    List<String> currentChildren = zk.getChildren(current, true);
                    currentChildren.forEach(ch -> toVisit.add(current + "/" + ch));

                } catch (KeeperException | InterruptedException e) {
                    e.printStackTrace();
                }

            }

            if(previousCount != results.size()) {
                listener.childNumberUpdate(results.size(), results);
                previousCount = results.size();
            }
        } else {
            listener.childNumberUpdate(-1, null);
        }
    }

    @Override
    public void processResult(int rc, String path, Object ctx, Stat stat) {
        boolean exists;
        switch (rc) {
            case Code.Ok:
                exists = true;
                break;
            case Code.NoNode:
                exists = false;
                break;
            case Code.SessionExpired:
            case Code.NoAuth:
                dead = true;
                listener.closing(rc);
                return;
            default:
                // Retry errors
                zk.exists(znode, true, this, null);
                return;
        }
        if (previousState ^ exists) {
            listener.setRunningState(exists);
            previousState = exists;
        }
    }
}